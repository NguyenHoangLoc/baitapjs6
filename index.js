//tìm số nguyên dương thỏa DK
function printNumber(){
    var n=0
    var sum=0
    while(sum<10000){ 
           n++       
           sum+=n      
    }
    document.getElementById("resultNumber").innerHTML=`Số Nguyên Dương Nhỏ Nhất Là:${n} `
}
// tính số mũ
function printSumExponent(){
    var soNguyen=document.getElementById("soNguyen").value*1
    var soMu=document.getElementById("soMu").value*1
    var i
    var sumExponent=0
    for(i=1;i<=soMu;i++) {
        var resultExponent=Math.pow(soNguyen,i)
        sumExponent+=resultExponent
    }
    document.getElementById("resultSum").innerHTML=`Kết quả bằng: ${sumExponent} `
}
//tính giai thừa
function countFactorial(){
   var number3=document.getElementById("number3").value*1
   var x
   var resultFactorial=1
    
   for(x=1; x<=number3;x++){
     resultFactorial*=x
   }
   if (number3>=0){
   document.getElementById("resultFactorial").innerHTML=`Kết quả: ${resultFactorial}`}
   else{
   document.getElementById("resultFactorial").innerHTML=" Không Tồn Tại Giai Thừa "
   }
}
//tạo div
function createDiv(){
    var divs=document.querySelectorAll(".listdiv div");
    for(var i=0;i<divs.length;i++){
        if((i+1)%2==0){
            divs[i].style.background="red"
            divs[i].style.color="white"
            divs[i].innerHTML="div chẵn"
        }else{
            divs[i].style.background="blue"
            divs[i].innerHTML="div lẻ"
            divs[i].style.color="white"
        }
    }
}
//in số nguyên tố 
function checkPrime(primeNumber){
    var i
    var sqrtNumber=Math.sqrt(primeNumber)
    if(i<2){
        return 0
    }
    for(i=2;i<=sqrtNumber;i++){
        if(primeNumber%i==0){
            return 0
        }
    }
    return 1
}
function printPrime(){
    var numberInput= document.getElementById("numberInput").value*1
    var i
    var printPrime=""
    if (numberInput>=2){
      printPrime=""+"2"
    }
    for (i=3;i<=numberInput;i+=2){
        if(checkPrime(i)==1){
      printPrime+=`  ${i}`  
        }

    }
    document.getElementById("primeValue").innerHTML=`Các Số Nguyên Tố: ${printPrime}`
}